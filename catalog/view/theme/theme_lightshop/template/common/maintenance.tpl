<?php echo $header; ?>
<style>body{overflow:hidden}.maintenance{position:fixed;top:0;left:0;z-index:100;background:rgba(0,0,0,0.7);color:#fff;width:100%;height:100%}.header__top,.header__nav,.header__actions,.footer{display:none}a.header__logo{float:none}.header__bottom{text-align:center}
</style>
<main class="content">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
			<div class="maintenance"><?php echo $message; ?></div>
			</div>
		</div>
	</div>
</main>
<?php echo $footer; ?></body></html>
