				<span class="popup-close js-popup-close"><svg class="icon-chevron-left"><use xlink:href="#chevron-small-left"></use></svg></span>
				<span class="popup__title"><?php echo $text_header_category; ?></span>
				<div class="popup__submenu-scroll">
				<div class="nav-submenu">
					<ul class="nav-submenu__list">					
								  <?php foreach ($main_navs as $key => $main_nav) { ?>
									   <?php if ($main_nav['settype'] == 0) { ?>
									     <?php if (!isset($main_nav['type'][$main_nav['settype']]['links'])) { $main_nav['type'][$main_nav['settype']]['links'] = $topcat; }?>
										 <?php  foreach ($main_nav['type'][$main_nav['settype']]['links'] as $key => $link) { ?> 
											<?php $cat_id = substr($key, 1); ?>									   
											  <?php if (!count($categories[$cat_id]['children'])) { ?>
											  <li class="nav-submenu__list-item">
												<a href="<?php echo $categories[$cat_id]['href']; ?>" class="nav-submenu__link"><?php echo key($top_links[$key]);  ?></a>
											  </li>
											  <?php }else{ ?>
												<li class="nav-submenu__list-item nav-submenu__item">
													<a href="#mnav<?php echo $key; ?>" class="nav-submenu__link nav-submenu__title js-submenu-link"><?php echo key($top_links[$key]);  ?><svg class="icon-chevron-right"><use xlink:href="#chevron-small-left"></use></svg></a>
													<ul class="nav-submenu__list nav-submenu__item">
													  <li class="nav-submenu__list-item nav-submenu__list-item--title"><svg class="icon-chevron-left"><use xlink:href="#chevron-small-left"></use></svg><?php echo key($top_links[$key]);  ?></li>
																											  
															  <?php  foreach ($categories[$cat_id]['children'] as  $category) { ?>	
																	<li class="nav-submenu__list-item nav-submenu__item">
																	  <?php if (count($category['children'])) { ?>
																		<a href="<?php echo $category['href']; ?>" class="nav-submenu__link nav-submenu__title js-submenu-link"><?php  echo $category['name']; ?><svg class="icon-chevron-right"><use xlink:href="#chevron-small-left"></use></svg></a>
																		<ul class="nav-submenu__list">
																			<li class="nav-submenu__list-item nav-submenu__list-item--title"><svg class="icon-chevron-left"><use xlink:href="#chevron-small-left"></use></svg><?php  echo $category['name']; ?></li>
																			  <?php  foreach ($category['children'] as  $category3) { ?>					
																				<li class="nav-submenu__list-item"><a href="<?php echo $category3['href']; ?>" class="nav-submenu__link"><?php  echo $category3['name']; ?></a></li>					
																			  <?php } ?>
																		</ul>
																	</li>
																	  <?php }else{ ?>
																		<li class="nav-submenu__list-item"><a href="<?php echo $category['href']; ?>" class="nav-submenu__link nav-submenu__title"><?php  echo $category['name']; ?></a></li>
																	  <?php } ?>													
															  <?php } ?>
													  
													</ul>
												</li>
											 <?php } ?>
										 <?php } ?>	
									   <?php } elseif($main_nav['settype'] == 1) { ?>
											<?php  if(!isset($main_nav['type'][$main_nav['settype']]['links'])) { $main_nav['type'][$main_nav['settype']]['links'] = array();}?>
											<li class="nav-submenu__list-item nav-submenu__item">
												 <a href="#mnav<?php echo $key; ?>" class="nav-submenu__link nav-submenu__title js-submenu-link"><?php echo $main_nav['type'][$main_nav['settype']]['language'][$language_id]['name'];  ?><svg class="icon-chevron-right"><use xlink:href="#chevron-small-left"></use></svg></a>
												<ul class="nav-submenu__list nav-submenu__item">										  
												  <li class="nav-submenu__list-item nav-submenu__list-item--title"><svg class="icon-chevron-left"><use xlink:href="#chevron-small-left"></use></svg><?php echo $main_nav['type'][$main_nav['settype']]['language'][$language_id]['name'];  ?></li>
												   
												   <?php  foreach ($main_nav['type'][$main_nav['settype']]['links'] as $id => $link) { ?>
													 
													 <li class="nav-submenu__list-item"><a href="<?php echo $link; ?>" class="nav-submenu__link nav-submenu__title"><?php echo (isset($top_links[$id])) ? key($top_links[$id]) : ''; ?></a></li>
												   <?php } ?>								   
												</ul>
											 </li>
									   <?php } elseif($main_nav['settype'] == 2) { ?>
											<?php  if(!isset($main_nav['type'][$main_nav['settype']]['links'])) { $main_nav['type'][$main_nav['settype']]['links'] = array();}?>
											<?php  foreach ($main_nav['type'][$main_nav['settype']]['links'] as $id => $link) { ?>
												<li "nav-submenu__list-item">
													<a href="<?php echo $link; ?>" class="nav-submenu__link"><?php echo (isset($top_links[$id])) ? key($top_links[$id]) : ''; ?></a>
												</li>
											<?php } ?>
									   <?php } ?>
								  <?php } ?>
								  <!-- MAIN NAV  -->					
					
					</ul>
					<div class="nav-submenu nav-submenu-about">
						<ul class="nav-submenu__list">						
							<?php  foreach ($header_navs as $key => $header_nav) { ?>
							   <?php if ($header_nav['settype']) { ?>
								<?php  foreach ($header_nav['type'][$header_nav['settype']]['links'] as $id => $link) { ?>
									<li class="nav-submenu__list-item"><a href="<?php echo $link; ?>" class="nav-submenu__link"><?php echo (isset($top_links[$id])) ? key($top_links[$id]) : ''; ?></a></li>
								<?php } ?>
							   <?php } else { ?>
							    <li class="nav-submenu__list-item nav-submenu__item">
								 <a href="#nav<?php echo $key; ?>" class="nav-submenu__link nav-submenu__title js-submenu-link"><?php echo $header_nav['type'][$header_nav['settype']]['language'][$language_id]['name']  ?><svg class="icon-chevron-right"><use xlink:href="#chevron-small-left"></use></svg></a>								
									  
									<ul class="nav-submenu__list">
									  <li class="nav-submenu__list-item nav-submenu__list-item--title"><svg class="icon-chevron-left"><use xlink:href="#chevron-small-left"></use></svg><?php echo $header_nav['type'][$header_nav['settype']]['language'][$language_id]['name']  ?></li>
									   <?php  foreach ($header_nav['type'][$header_nav['settype']]['links'] as $id => $link) { ?>
										 <li class="nav-submenu__list-item"><a href="<?php echo $link; ?>" class="nav-submenu__link"><?php echo (isset($top_links[$id])) ? key($top_links[$id]) : ''; ?></a></li>
									   <?php } ?>
									</ul>								  
								</li>
							   <?php } ?>
							<?php } ?>				
						</ul>
					</div>
				</div>
				</div>