<?php echo $header; $col = $column_left ? 9 : 12; $col = $column_right ? $col - 3 : $col; ?>
<div class="alerts">
<?php if ($attention) { ?>
	<div class="alert alert--yellow">
		<span class="alert__text">
			<?php echo $attention; ?>
		</span>
		<a href="#" class="alert__close">x</a>
	</div>
<?php } ?>
<?php if ($success) { ?>
	<div class="alert alert--green">
		<span class="alert__text">
			<?php echo $success; ?>
		</span>
		<a href="#" class="alert__close">x</a>
	</div>
<?php } ?>
<?php if ($error_warning) { ?>
	<div class="alert alert--red">
		<span class="alert__text">
			<?php echo $error_warning; ?>
		</span>
		<a href="#" class="alert__close">x</a>
	</div>
<?php } ?>
</div>
<main class="content">
	<div class="container">
	<?php echo $content_top; ?>
		<div class="breadcrumbs breadcrumbs--sm-pad">
			<ul class="breadcrumb__list">
			<?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
			<?php if($i == 0) { ?>
				<li <?php echo $schema ? 'itemscope itemtype="http://data-vocabulary.org/Breadcrumb"' : ''?> class="breadcrumb__list-item"><a href="<?php echo $breadcrumb['href']; ?>" <?php echo $schema ? 'itemprop="url"' : ''?>><span <?php echo $schema ? 'itemprop="title"' : ''?>><?php echo $breadcrumb['text']; ?></span></a></li>
			<?php } elseif($i + 1 < count($breadcrumbs)) { ?>
				<li <?php echo $schema ? 'itemscope itemtype="http://data-vocabulary.org/Breadcrumb"' : ''?> class="breadcrumb__list-item"><svg class="icon-chevron-right"><use xlink:href="#chevron-small-left"></use></svg><a href="<?php echo $breadcrumb['href']; ?>" <?php echo $schema ? 'itemprop="url"' : ''?>><span <?php echo $schema ? 'itemprop="title"' : ''?>><?php echo $breadcrumb['text']; ?></span></a></li>		
			<?php } else { ?>
				<li class="breadcrumb__list-item"><svg class="icon-chevron-right"><use xlink:href="#chevron-small-left"></use></svg><?php echo $breadcrumb['text']; ?></li>		
			<?php } ?>
			<?php } ?> 			
			</ul>
		</div>
	<h1 class="content__title"><?php echo $heading_title; if ($weight) { echo ' (' . $weight . ')'; };  ?></h1>
	<div class="row">
		<?php echo $column_left ? '<div class="col-sm-3">' . $column_left . '</div>' : ''?>
			<div class="col-sm-<?php echo $col; ?>">
				<div class="cart">
					<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="cartcontent">	 
					<div class="cart__products">
						<table class="cart__table">
						<?php foreach ($products as $product) { ?>
							<tr class="cart__table-row">
							<?php if ($product['thumb']) { ?>
								<td class="cart__table-photo">
									<a href="<?php echo $product['href']; ?>" class="cart__photo">
										<img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>">
									</a>
								</td>
							<?php } ?>	
								<td <?php if (!$product['thumb']) { ?>colspan="2"<?php } ?>	 class="cart__table-name">
										<a href="<?php echo $product['href']; ?>" class="cart__product-name">
											<?php echo $product['name']; ?>
										</a>
										<?php if (!$product['stock']) { ?>
										***
										<?php } ?>
								</td>
								<td class="cart__table-discount">
									<span class="cart__discount">
										<?php echo $product['model']; ?><br>
										
										<?php if ($product['option']) { ?>
											<?php foreach ($product['option'] as $option) { ?>					 
												<?php echo $option['name']; ?>: <?php echo $option['value']; ?><br>
											<?php } ?>
										<?php } ?>
									  
										<?php if ($product['reward']) { ?>
											<?php echo $product['reward']; ?><br>
										<?php } ?>
										
										<?php if ($product['recurring']) { ?>
											<?php echo $text_recurring_item; ?> <?php echo $product['recurring']; ?><br>
										<?php } ?>	
									</span>
								</td>
								<td class="cart__table-price">
									<span class="catalogue__price catalogue__price--md catalogue__price--think" title="<?php echo $column_price; ?>">
										<?php echo $product['price']; ?>
									</span>
								</td>					
								<td class="cart__table-spinner">
									<div class="spinner-wrap">
										<input name="quantity[<?php echo $product['cart_id']; ?>]" type="text" class="spinner" value='<?php echo $product['quantity']; ?>' placeholder="<?php echo $product['quantity']; ?>" >
									</div>
								</td>

								<td class="cart__table-price-total">
									<span class="catalogue__price catalogue__price--md catalogue__price--total" title="<?php echo $column_total; ?>">
										<?php echo $product['total']; ?>
									</span>
								</td>
								<td class="cart__table-delete">
									<a onclick="cart.remove('<?php echo $product['cart_id']; ?>');" class="cart__delete" title="<?php echo $button_remove; ?>">
										<svg class="icon-cross">
											<use xlink:href="#cross"></use>
										</svg>
									</a>
								</td>
							</tr>
							<?php } ?>
					  <?php foreach ($vouchers as $voucher) { ?>
							<tr class="cart__table-row">
								<td colspan="5" class="cart__table-name"><?php echo $voucher['description']; ?></td>
								<td class="cart__table-price-total">
									<span class="catalogue__price catalogue__price--md catalogue__price--total" title="<?php echo $column_total; ?>">
										<?php echo $voucher['amount']; ?>
									</span>
								</td>
								
								<td class="cart__table-delete">
									<a onclick="voucher.remove('<?php echo $voucher['key']; ?>');" class="cart__delete" title="<?php echo $button_remove; ?>">
										<svg class="icon-cross">
											<use xlink:href="#cross"></use>
										</svg>
									</a>
								</td>
							</tr>
					  <?php } ?>					
						</table>
					</div>
					</form>
					
					<div class="cart__meta">	  
						<?php if ($modules) { ?>
						<div class="product-info cart--tabs js-product-info js-cart-tabs"> 
							<div class="js-tabs-box js-tabs-box-product">
								<div class="tabs">
									<ul class="tabs__list js-cart-tabs-list"></ul>
								</div>
								<div class="product-info__tabs-container js-cart-tabs-container"></div>
							</div>	
						</div>	
						<div class="js-cart-tabs-modules">
							<?php foreach ($modules as $module) { ?>
							<?php echo $module; ?>
							<?php } ?>
						</div>		
						<?php } ?>
						<div class="cart__sum">
						<?php foreach ($totals as $i=> $total) { ?>
							<?php if($i + 1 < count($totals)) { ?>
								<span class="cart__sum-text">
									<?php echo $total['title']; ?>:
									<span class="cart__sum-text-price">
										<?php echo $total['text']; ?>
									</span>
								</span><br>			
							<?php } else { ?>
								<span class="cart__sum-text">
									<?php echo $total['title']; ?>:
									<span class="cart__sum-price">
										<?php echo $total['text']; ?>
									</span>
								</span><br>				
							<?php }?>
						<?php } ?>				
						</div>
					</div>
					<div class="cart__buttons">
						<div class="cart__buttons-left">
							<a href="<?php echo $continue; ?>" class="btn btn--transparent">
								<?php echo $button_shopping; ?>
							</a>
						</div>
						<div class="cart__buttons-right">
							<span class="cart__buttons-right-btn">
								<a href="<?php echo $checkout; ?>" class="btn"><?php echo $button_checkout; ?></a>
							</span>
							<?php if ($buy_click['status']) { ?>
								<button type="button" href="#popup-buy-click" data-typefrom="cart-popup" data-for="" class="btn btn--link js-fancy-popup-cart" style="min-width: inherit;"><?php echo $text_lightshop_fast_order; ?></button>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
			<?php echo $column_right ? '<div class="col-sm-3">' . $column_right . '</div>' : ''?>
		</div>	
		<?php echo $content_bottom; ?>
	</div>
</main>
		<?php if ($buy_click['status']) { ?>
		<div class="popup-simple" id="popup-buy-click">
			<div class="popup-simple__inner" >
				<form id="data">
				<h3><?php echo $text_lightshop_fast_order; ?> </h3>
					<?php require( DIR_TEMPLATE . 'theme_lightshop/template/product/buyclick_form.tpl' ); ?>
					<button type="button" class="btn js-btn-add-cart-fast quickbuy-send"><?php echo $button_fastorder_sendorder; ?></button>
					<input name="redirect"  value="1" id="fast-redirect" type="hidden">
					<?php if ($text_lightshop_pdata) { ?>
					<div class="popup-simple__inner-personal-data"><?php echo $text_lightshop_pdata; ?></div>
					<?php } ?>
				</form>
			</div>	
		</div>
		<?php } ?>
<?php echo $footer; ?></body></html>