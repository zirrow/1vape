<?php echo $header; $col = $column_left ? 9 : 12; $col = $column_right ? $col - 3 : $col; ?>
<main class="content">
	<div class="container">
	<?php echo $content_top; ?>
		<div class="breadcrumbs breadcrumbs--sm-pad">
			<ul class="breadcrumb__list">
			<?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
			<?php if($i == 0) { ?>
				<li <?php echo $schema ? 'itemscope itemtype="http://data-vocabulary.org/Breadcrumb"' : ''?> class="breadcrumb__list-item"><a href="<?php echo $breadcrumb['href']; ?>" <?php echo $schema ? 'itemprop="url"' : ''?>><span <?php echo $schema ? 'itemprop="title"' : ''?>><?php echo $breadcrumb['text']; ?></span></a></li>
			<?php } elseif($i + 1 < count($breadcrumbs)) { ?>
				<li <?php echo $schema ? 'itemscope itemtype="http://data-vocabulary.org/Breadcrumb"' : ''?> class="breadcrumb__list-item"><svg class="icon-chevron-right"><use xlink:href="#chevron-small-left"></use></svg><a href="<?php echo $breadcrumb['href']; ?>" <?php echo $schema ? 'itemprop="url"' : ''?>><span <?php echo $schema ? 'itemprop="title"' : ''?>><?php echo $breadcrumb['text']; ?></span></a></li>		
			<?php } else { ?>
				<li class="breadcrumb__list-item"><svg class="icon-chevron-right"><use xlink:href="#chevron-small-left"></use></svg><?php echo $breadcrumb['text']; ?></li>		
			<?php } ?>
			<?php } ?> 			
			</ul>
		</div>
    <h1 class="content__title"><?php echo $heading_title; ?></h1>
	</div>
	
	<?php if ($locations) { ?>
	
	<div class="locations-select">
		<div class="container">
		<select id="js-geocode" class="select--squer select--map select" data-placeholder="<?php echo $text_store; ?>">
			<option><?php echo $text_store; ?></option>
			<option value="<?php echo $geocode; ?>" ><?php echo $store; ?></option>
			<?php foreach ($locations as $location) { ?>
			<option value="<?php echo $location['geocode']; ?>"><?php echo $location['name']; ?></option>
			<?php } ?>
		</select>
		</div>
	</div>
	<?php } ?>
	<div class="js-map" id="js-map"></div>
	
<div <?php echo $schema ? 'itemscope itemtype="http://schema.org/Organization"' : ''?> class="contacts">
<?php echo $schema ? '<meta itemprop="name" content="' . $store . '">' : ''?>
	<div class="container">
	<div class="row">
		<?php echo $column_left ? '<div class="col-sm-3">' . $column_left . '</div>' : ''?>
		<div class="col-sm-<?php echo $col; ?>">	
		<div class="contacts__info">
			<div class="contacts__info-inner">
				<div <?php echo $schema ? 'itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"' : ''?> class="contacts__info-item">
					<span class="contacts__info-icon">
						<svg class="icon-location-pin">
							<use xlink:href="#location-pin"></use>
						</svg>
					</span>
					<address <?php echo $schema ? 'itemprop="streetAddress"' : ''?> class="contacts__address">
						<?php echo $address; ?>
					</address>
				</div>
				<?php if ($phone_1 || $phone_2) { ?>
				<div class="contacts__info-item">
					<span class="contacts__info-icon">
						<svg class="icon-phone">
							<use xlink:href="#phone"></use>
						</svg>
					</span>
					<div class="contacts__numbers">
					<?php if ($phone_1) { ?>
						<a <?php echo $schema ? 'itemprop="telephone"' : ''; echo 'href="tel:' . preg_replace('/[^0-9]/', '', $phone_1) . '" rel="nofollow"';?> class="contacts__numbers-item">
							<?php echo $phone_1; ?>
						</a>
					<?php } ?>
					<?php if ($phone_2) { ?>
						<a <?php echo $schema ? 'itemprop="telephone"' : ''; echo 'href="tel:' . preg_replace('/[^0-9]/', '', $phone_2) . '" rel="nofollow"';?> class="contacts__numbers-item">
							<?php echo $phone_2; ?>
						</a>
					<?php } ?>
					</div>
				</div>
				<?php } ?>
				<?php if ($shop_email) { ?>
				<div class="contacts__info-item">
					<span class="contacts__info-icon">
						<svg class="icon-mail">
							<use xlink:href="#mail"></use>
						</svg>
					</span>
					<div class="contacts__info-text">
						<span class="contacts__info-title">
							<?php echo $text_lightshop_support; ?>
						</span>
						<a href="mailto:<?php echo $shop_email; ?>" class="contacts__mail">
							<span <?php echo $schema ? 'itemprop="email"' : ''?>><?php echo $shop_email; ?></span>
						</a>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>

		<?php  if(isset($social_navs)) { ?>			  
		<div class="contacts__social">
			<span class="contacts__social-title">
				<?php echo $text_lightshop_con_soc; ?>
			</span>
			<ul class="social contacts__social-list">
				<?php  foreach ($social_navs as $key => $social_nav) { ?>
				<li class="social__item">
					<a href="<?php echo $social_nav['link']; ?>" target="<?php echo $social_nav['attr']; ?>" class="social__link">
						<svg class="icon-fb icon-fb--lg">
							<use xlink:href="#<?php echo strtolower($social_links[$social_nav['settype']]); ?>"></use>
						</svg>
					</a>
				</li>
				<?php } ?>
			</ul>
		</div>
		<?php } ?>
<div style="display:none;"><a href="#contact-form"  id="contact_popup" class="btn btn--transparent js-fancy-popup"><?php echo $text_contact; ?></a></div>		
		<div class="popup-simple" id="contact-form">
			<div class="popup-simple__inner">
				 <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
					<h3><?php echo $text_contact; ?></h3>
					<input name="name" id="input-name" type="text" placeholder="* <?php echo $entry_name; ?>" value="<?php echo $name; ?>">
              <?php if ($error_name) { ?>
              <div class="popup-simple__inner-error-text"><?php echo $error_name; ?></div>
              <?php } ?>
					<input name="email" id="input-email" type="text" placeholder="* <?php echo $entry_email; ?>" value="<?php echo $email; ?>">
              <?php if ($error_email) { ?>
              <div class="popup-simple__inner-error-text"><?php echo $error_email; ?></div>
              <?php } ?>
					<textarea rows="5" name="enquiry" id="input-enquiry" placeholder="* <?php echo $entry_enquiry; ?>"><?php echo $enquiry; ?></textarea>
              <?php if ($error_enquiry) { ?>
              <div class="popup-simple__inner-error-text"><?php echo $error_enquiry; ?></div>
              <?php } ?>
					<?php echo $captcha; ?>
					<button type="submit" id="contact_send" class="btn"><?php echo $button_submit; ?></button>
				<?php if ($text_lightshop_pdata) { ?>
					<div class="popup-simple__inner-personal-data"><?php echo $text_lightshop_pdata; ?></div>
				<?php } ?>
				</form>			
			</div>
		</div>
		
		</div>
		<?php echo $column_right ? '<div class="col-sm-3">' . $column_right . '</div>' : ''?>
	</div>			
	</div>
</div>

	<div class="container">
	<?php echo $content_bottom; ?>
	</div>
</main>
<?php echo $footer; ?>
<script>

jQuery.getScript('https://maps.googleapis.com/maps/api/js?key=<?php echo $api_key; ?>&sensor=false&extension=.js', function (){
	
	function LightshopMarker(latlng, map, args) {
		this.latlng = latlng;	
		this.args = args;	
		this.setMap(map);	
		//fancyPopUp();
	}

	LightshopMarker.prototype = new google.maps.OverlayView();

	LightshopMarker.prototype.draw = function() {
		var self = this;
		var div = this.div;
		
		if (!div) {
			div = this.div = document.createElement('div');
			div.className = 'contacts__marker';
			div.style.position = 'absolute';
			div.style.cursor = 'default';
			div.innerHTML = self.args.m_html;
			if (typeof(self.args.marker_id) !== 'undefined') {
				div.dataset.marker_id = self.args.marker_id;
			}
			var panes = this.getPanes();
			panes.overlayImage.appendChild(div);
		}

		var point = this.getProjection().fromLatLngToDivPixel(this.latlng);

		if (point) {
		div.style.left = (point.x + 10) + 'px';
		div.style.top = (point.y - 30) + 'px';
		}
		fancyPopUp();
	};

	google.maps.Map.prototype.LightshopCenterOffset= function(latlng, offsetX, offsetY) {
		var map = this;
		var ov = new google.maps.OverlayView(); 
		ov.onAdd = function() { 
			var proj = this.getProjection(); 
			var aPoint = proj.fromLatLngToContainerPixel(latlng);
			aPoint.x = aPoint.x+offsetX;
			aPoint.y = aPoint.y+offsetY;
			map.setCenter(proj.fromContainerPixelToLatLng(aPoint));
		}
		ov.draw = function() {}; 
		ov.setMap(this); 
	};

	var map;

	function initialize() {
		 	map = new google.maps.Map(document.getElementById('js-map'), {
				zoom: <?php echo $zoom; ?>,
				zoomControl: <?php echo $zoom_control ? 'true' : 'false' ?>,
				disableDoubleClickZoom: true,
				mapTypeControl: false,
				scaleControl: false,
				scrollwheel: false,
				panControl: false,
				streetViewControl: false,
				draggable : true,
				center: new google.maps.LatLng(<?php echo $geocode; ?>),
			});
			
			map.LightshopCenterOffset(new google.maps.LatLng(<?php echo $geocode; ?>), 143, 200);

			var overlay = new LightshopMarker(
				new google.maps.LatLng(<?php echo $geocode; ?>), 
				map,
				{
					//marker_id: 'id-0',
					m_html: '<div class="contacts__marker-inner">' +
'<div class="contacts__marker-item"><div class="contacts__marker-adress"><span class="contacts__marker-icon"><svg class="icon-location-pin"><use xlink:href="#location-pin"></use></svg><?php echo str_replace(array("\r", "\n"), '', $address); ?></span></div></div>' +
<?php if ($open) { ?>
'<div class="contacts__marker-item"><div class="contacts__marker-open"><?php echo str_replace(array("\r", "\n"), '', $open); ?></div></div>' +
<?php } ?>
'<div class="contacts__marker-item"><div class="contacts__marker-telephone"><?php echo $telephone; ?></div></div>' +
<?php if ($fax) { ?>
'<div class="contacts__marker-item"><div class="contacts__marker-fax"><?php echo $fax; ?></div></div>' +
<?php } ?>
<?php if ($comment) { ?>
'<div class="contacts__marker-item"><div class="contacts__marker-comment"><?php echo str_replace(array("\r", "\n"), '', $comment); ?></div></div>' +
<?php } ?>
'<div class="contacts__marker-btn"><a href="#contact-form" class="btn btn--transparent js-fancy-popup"><?php echo $text_contact; ?></a></div>' +
'</div>'
				}
			);
			
<?php if ($locations) { ?>
<?php foreach ($locations as $location) { ?>
			var overlay = new LightshopMarker(
				new google.maps.LatLng(<?php echo $location['geocode']; ?>), 
				map,
				{
					//marker_id: 'id-<?php echo $location['location_id']; ?>',
					m_html: '<div class="contacts__marker-inner">' +
'<div class="contacts__marker-item"><div class="contacts__marker-adress"><span class="contacts__marker-icon"><svg class="icon-location-pin"><use xlink:href="#location-pin"></use></svg><?php echo str_replace(array("\r", "\n"), '', $location['address']); ?></span></div></div>' +
<?php if ($location['open']) { ?>
'<div class="contacts__marker-item"><div class="contacts__marker-open"><?php echo str_replace(array("\r", "\n"), '', $location['open']); ?></div></div>' +
<?php } ?>
'<div class="contacts__marker-item"><div class="contacts__marker-telephone"><?php echo $location['telephone']; ?></div></div>' +
<?php if ($location['fax']) { ?>
'<div class="contacts__marker-item"><div class="contacts__marker-fax"><?php echo $location['fax']; ?></div></div>' +
<?php } ?>
<?php if ($location['comment']) { ?>
'<div class="contacts__marker-item"><div class="contacts__marker-comment"><?php echo str_replace(array("\r", "\n"), '', $location['comment']); ?></div></div>' +
<?php } ?>
'<div class="contacts__marker-btn"><a href="#contact-form" class="btn btn--transparent js-fancy-popup"><?php echo $text_contact; ?></a></div>' +
'</div>'
				}
			);
<?php } ?>
<?php } ?>

	}

	google.maps.event.addDomListener(window, 'load', initialize);

	jQuery(document).on('change','#js-geocode',function() {
		var latlng = jQuery(this).val().split(','),
			newlat = 1*latlng[0],
			newlng = 1*latlng[1];
			
			map.LightshopCenterOffset(new google.maps.LatLng(newlat, newlng), 143, 200);
	});
});

$(document).on('ready', function() {
	var $errors = $('#contact-form .popup-simple__inner-error-text') ;
	if($errors.length){
		console.log($errors.length);
		$("#contact_popup").trigger('click');
	}
})

</script></body></html>