 <!-- CATALOGUE PRODUCT DETAIL-->

	<!-- CATALOGUE PRODUCT DETAIL TABS CONTAINER -->
											

	<div class="catalogue__tabs-container">
		<div class="catalogue__product-detail-top">
			<a href="<?php echo $popup_link; ?>" id="popup_link_prod" target="_blank" class="new-window-link">
				<?php echo $text_lightshop_popup_link; ?>
			</a>
			<a href="#" class="catalogue__block-hide js-product-view-hide">
				<svg class="icon-chevron-bottom"> 
					<use xlink:href="#chevron-small-left"></use>
				</svg>
			</a>
		</div>
		<div class="js-custom-scroll">
			<div class="scroll-container">
				
				<div class="catalogue__tabs-container-inner">
					<?php foreach ($products as $product) { ?>
						<?php if ($product['product_id'] == $product_id) { ?>
					<div class="catalogue__product-detail-main  js-tab-content active" id="popupprodid_<?php echo $product['product_id']; ?>">
						<?php } else { ?>
					<div class="catalogue__product-detail-main js-tab-content" data-page="<?php echo $page; ?>" id="popupprodid_<?php echo $product['product_id']; ?>">
						<?php } ?>
					  
						<div class="catalogue__product-detail-gallery">
							<input type="hidden" name="product_href" value="<?php echo $product['href']; ?>">
							<div class=" arrows-container"></div>
							<div  class="catalogue__product-detail-gallery-inner js-simple-slider">

								<div class="catalogue__product-detail-gallery-item">
									<img data-lazy="<?php echo $product['thumb']; ?>" src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs%3D" alt="<?php echo $product['name']; ?>">
									<?php if($product['isnewest']) { ?>
										<span class="label catalogue__product-detail-label"><?php echo $labelsinfo['new']['name'][$language_id]; ?></span>
									<?php } ?>
									<?php if($product['special'] && $product['sales']) { ?>
										<span class="label catalogue__product-detail-label label--red"><?php echo $labelsinfo['sale']['name'][$language_id] . ' ' . $product['discount']; ?></span>
									<?php } ?>
									<span onclick="wishlist.add('<?php echo $product_id; ?>');" class="products-list__like catalogue__product-detail-like">
										<svg class="icon-heart icon-heart--gray">
											<use xlink:href="#heart"></use>
										</svg>
									</span>
								</div>

							  <?php  foreach ($product['images'] as $image) { ?>
								<div class="catalogue__product-detail-gallery-item">
									<img data-lazy="<?php echo $image; ?>" src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs%3D" alt="<?php echo $product['name']; ?>" >
									<?php if($product['isnewest']) { ?>
										<span class="label catalogue__product-detail-label"><?php echo $labelsinfo['new']['name'][$language_id]; ?></span>
									<?php } ?>
									<span class="products-list__like catalogue__product-detail-like">
										<svg class="icon-heart icon-heart--gray">
											<use xlink:href="#heart"></use>
										</svg>
									</span>
								</div>
							  <?php  } ?>
							</div>
						</div>
						<?php // foreach ($products as $product) { ?>
							<form>
						<div class="catalogue__product-detail-info">						
							<div class="catalogue__product-detail-info-left">
								<div class="catalogue__product-detail-info-top">
									<span class="catalogue__product-detail-series">
										<?php echo $product['manufacturer']; ?>
									</span>
									<?php if ($review_status) { ?>
									<div class="catalogue__product-detail-rating">
										<div class="product__rating catalogue__rating">
											<div class="product__rating-fill" style="width: <?php echo $product['rating']*20; ?>%;">	
											</div>
										</div>
									</div>
									<?php } ?>
								</div>
								<span class="catalogue__product-name catalogue__product-name--lgx catalogue__product-detail-name">
									<?php echo $product['name']; ?>
								</span>
								<input type="hidden" name="product_id" value="<?php echo $product['product_id']; ?>">
								<span class="catalogue__product-detail-compare">
									<a onclick="compare.add('<?php echo $product_id; ?>');" class="products-list__compare">
										<span class="products-list__compare-icon">
											<svg class="icon-add-to-list">
												<use xlink:href="#add-to-list"></use>
											</svg>
										</span>
										<span class="products-list__compare-text">
											<?php echo $button_compare; ?>
										</span>
									</a>
								</span>
								<div class="catalogue__product-detail-description">
									<p class="catalogue__product-detail-text">
										<?php echo $product['description']; ?>
									</p>
									<a href="<?php echo $product['href']; ?>" target="_blank"><?php echo $text_lightshop_popup_link_more; ?></a>
								</div>

					<?php if (isset($product['customTabs']['popup'])) { ?>
						<div class="catalogue__product-detail-popup">
							<?php foreach ($product['customTabs']['popup'] as $key => $popup) { ?>
								<a href="index.php?route=product/product/custtabload&product_ids=<?php echo $product['product_id']; ?>&tab=<?php echo $key; ?>" class="product-page__actions-link--dashed js-fancy-tab"><?php echo $popup['title'] ; ?></a>
							<?php } ?>
						</div>
					<?php } ?>


								<?php if ($product['options']) { ?>
<style>
/* options css */
<?php	foreach ($product['options'] as $option) { 
			if ($option['type'] == 'radio' || $option['type'] == 'checkbox') { 
				foreach ($option['product_option_value'] as $option_value) { 
					if ($option_value['image']) { ?>
.radiobox-colors__list-item input[type='radio']+ label.label--img-<?php echo $option_value['product_option_value_id']; ?>:before, .radiobox-colors__list-item input[type='checkbox']+ label.label--img-<?php echo $option_value['product_option_value_id']; ?>:before {
	background-image: url(<?php echo $option_value['image']; ?>);
}
<?php				} 
				} 
			} 
		} ?>
</style>

								<div class="catalogue__product-detail-filter">
									<?php foreach ($product['options'] as $i=> $option) { ?>
										<?php if ($option['type'] == 'select') { ?>
											<div class="catalogue__product-detail-option">
												<span class="catalogue__product-detail-filter-title">
													<?php echo $option['name']; ?>
												</span>												
												<span class="select">
													<select  name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="select" data-placeholder="<?php echo $text_select; ?>">
														<option value=""><?php echo $text_select; ?></option>
														<?php foreach ($option['product_option_value'] as $option_value) { ?>
															<option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
															</option>
														<?php } ?>
													</select>
												</span>
											</div>
										<?php } ?>
										<?php if ($option['type'] == 'radio') { ?>
											<div class="catalogue__product-detail-option">
												<span class="catalogue__product-detail-filter-title">
													<?php echo $option['name']; ?>
												</span>
												<div class="radiobox-colors">
													<form>
														<ul class="radiobox-colors__list">
												<?php foreach ($option['product_option_value'] as $option_value) { ?>
													<?php if ($option_value['image']) { ?>
														<li class="radiobox-colors__list-item option-value-id-<?php echo $option_value['option_value_id']; ?>">
															<input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" id="close-type-<?php echo $option_value['product_option_value_id'] . '-' . $option['product_option_id']; ?>">
															<label title="<?php echo $option_value['name']; ?>" for="close-type-<?php echo $option_value['product_option_value_id'] . '-' . $option['product_option_id']; ?>" class="label label--img-<?php echo $option_value['product_option_value_id']; ?>"></label>
														</li>
													<?php } else { ?>
														<li class="catalogue__type-item option-value-id-<?php echo $option_value['option_value_id']; ?>">
															<input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" id="close-type-<?php echo $option_value['product_option_value_id'] . '-' . $option['product_option_id']; ?>">
															<label for="close-type-<?php echo $option_value['product_option_value_id'] . '-' . $option['product_option_id']; ?>">
															<?php echo $option_value['name']; ?>		
															</label>
														</li>
													<?php } ?>
												<?php } ?>
														</ul>
													</form>
												</div>
											</div>
										<?php } ?>
										<?php if ($option['type'] == 'checkbox') { ?>
											<div class="catalogue__product-detail-option">
												<span class="catalogue__product-detail-filter-title">
													<?php echo $option['name']; ?>
												</span>
												<div class="radiobox-colors">
													<form>
														<ul class="radiobox-colors__list">
												<?php foreach ($option['product_option_value'] as $option_value) { ?>
													<?php if ($option_value['image']) { ?>
														<li class="radiobox-colors__list-item option-value-id-<?php echo $option_value['option_value_id']; ?>">
															<input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" id="close-type-<?php echo $option_value['product_option_value_id'] . '-' . $option['product_option_id']; ?>">
															<label title="<?php echo $option_value['name']; ?>" for="close-type-<?php echo $option_value['product_option_value_id'] . '-' . $option['product_option_id']; ?>" class="label label--img-<?php echo $option_value['product_option_value_id']; ?>"></label>
														</li>
													<?php } else { ?>
													
														<li class="catalogue__type-item option-value-id-<?php echo $option_value['option_value_id']; ?>">
															<input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" id="close-type-<?php echo $option_value['product_option_value_id'] . '-' . $option['product_option_id']; ?>">
															<label for="close-type-<?php echo $option_value['product_option_value_id'] . '-' . $option['product_option_id']; ?>">
															<?php echo $option_value['name']; ?>		
															</label>
														</li>
													<?php } ?>
												<?php } ?>
														</ul>
													</form>
												</div>
											</div>
										<?php } ?>
										<?php if ($option['type'] == 'text') { ?>
										<div class="catalogue__product-detail-option">
											<span class="catalogue__product-detail-filter-title"><?php echo ($option['required'] ? '<span style="color: red;">* </span>' : ''); ?><?php echo $option['name']; ?></span>
											<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>">
										</div>
										<?php } ?>
										<?php if ($option['type'] == 'textarea') { ?>
										<div class="catalogue__product-detail-option">
											<span class="catalogue__product-detail-filter-title"><?php echo ($option['required'] ? '<span style="color: red;">* </span>' : ''); ?><?php echo $option['name']; ?></span>
											<textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['value']; ?></textarea>
										</div>
										<?php } ?>
										<?php if ($option['type'] == 'file') { ?>
										<div class="catalogue__product-detail-option">
											<span class="catalogue__product-detail-filter-title"><?php echo ($option['required'] ? '<span style="color: red;">* </span>' : ''); ?><?php echo $option['name']; ?></span>
											<button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" class="btn btn--transparent btn--sm"><?php echo $text_lightshop_popup_upload; ?></button>
											<input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
										</div>
										<?php } ?>
										<?php if ($option['type'] == 'date') { ?>

										<div class="catalogue__product-detail-option">
											<span class="catalogue__product-detail-filter-title"><?php echo ($option['required'] ? '<span style="color: red;">* </span>' : ''); ?><?php echo $option['name']; ?></span>
											<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="date">
										</div>
										<?php } ?>
										<?php if ($option['type'] == 'datetime') { ?>

										<div class="catalogue__product-detail-option">
											<span class="catalogue__product-detail-filter-title"><?php echo ($option['required'] ? '<span style="color: red;">* </span>' : ''); ?><?php echo $option['name']; ?></span>
											<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="datetime">
										</div>
										<?php } ?>
										<?php if ($option['type'] == 'time') { ?>

										<div class="catalogue__product-detail-option">
											<span class="catalogue__product-detail-filter-title"><?php echo ($option['required'] ? '<span style="color: red;">* </span>' : ''); ?><?php echo $option['name']; ?></span>
											<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="time">
										</div>
										<?php } ?>
									<?php } ?>
								</div>
								<?php } ?>
							</div>
							<div class="catalogue__product-detail-info-right">
								<div class="catalogue__sell-info products-qty-info">
									<div class="catalogue__sell-info-top">
										<div class="catalogue__product-detail-price">
											<?php if ($product['special']) { ?>
												<span class="catalogue__price-old">
													<?php echo $product['price']; ?>
												</span>
												<span class="catalogue__price catalogue__price--lg">
													<?php echo $product['special']; ?>
												</span>
											<?php } else { ?>
												<span class="catalogue__price catalogue__price--lg">
													<?php echo $product['price']; ?>
												</span>												
											<?php } ?>
										</div>
										<span class="status <?php echo $product['quantity'] > 0 ? 'instock' : ''?>">
											<?php echo $product['stock']; ?>
										</span>
									</div>
									<div class="catalogue__sell-info-bottom">
										<div class="catalogue__sell-info-btns">
											<div class="catalogue__sell-info-item products-qty-info-spinner">
												<div class="spinner-wrap">
													<input name="quantity" type="text" class="spinner" value='1' placeholder="1">
												</div>
											</div>
											<div class="catalogue__sell-info-item">
												<button for="<?php echo $product['product_id']; ?>" type="button" class="btn products-full-list__btn js-btn-add-cart-popup"><?php echo $button_cart; ?></button>
											</div>
										</div>
										<?php if ($buy_click['status']) { ?>
										<a href="#popup-buy-click" data-typefrom="category-popup" data-for="<?php echo $product['product_id']; ?>" class="products-full-list__action-link js-fancy-popup-cart catalogue__product-detail-action-link"><?php echo $text_lightshop_buy_click; ?></a>
										<?php } ?>
									</div>
								</div> 
							</div>						
						</div>
						</form>
						<?php // } ?>
						
					</div>
					<?php  } ?>
				</div>
			</div>
		</div>
	</div>
	<!-- CATALOGUE PRODUCT DETAIL TABS CONTAINER END -->
	<!-- CATALOGUE PRODUCT DETAIL TABS LINKS -->
	<div class="catalogue__product-detail-side">
		<div class="product-tabs">
			<ul class="product-tabs__list">
				<?php if (isset($prevLink) && !$prodkey ) { ?>
					<li class="product-tabs__list-item js-tab-tabpopuplink">
						<a href="<?php echo $prevLink; ?>" class="product-tabs__link product-tabs__link--prevlast">
							<svg class="icon-chevron-left"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#chevron-small-left"></use></svg>
						</a>
					</li>
				<?php } ?>
				<?php foreach ($products as $key => $product) { ?>
						<?php if ($product['product_id'] == $product_id) { ?>
				<li class="product-tabs__list-item js-tab-tabpopup active" id="popupprodtabid_<?php echo $product['product_id']; ?>">
						<?php } else { ?>
				<li class="product-tabs__list-item js-tab-tabpopup" <?php echo ($key < $minVisKey) ?  "style='display:none'":"" ; ?> id="popupprodtabid_<?php echo $product['product_id']; ?>">							
						<?php } ?>
					<a href="#" class="product-tabs__link">
						<div class="product-tabs__photo">
							<img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>">
						</div>
						<div class="product-tabs__description">
							<span class="product-tabs__name" title="<?php echo $product['name']; ?>">
								<span class="product-tabs__name-inner"><?php echo $product['name']; ?></span>
							</span>
							<?php if ($product['special']) { ?>
								<span class="product-tabs__price-old">
									<?php echo $product['price']; ?>
								</span>
								<span class="product-tabs__price">
									<?php echo $product['special']; ?>
								</span>
							<?php } else { ?>
								<span class="product-tabs__price">
									<?php echo $product['price']; ?>
								</span>												
							<?php } ?>												
						</div>
					</a>
				</li>
				<?php } ?>
				<?php if (isset($nextLink) && isset($isLast)) { ?>
					<li class="product-tabs__list-item js-tab-tabpopuplink">
						<a href="<?php echo $nextLink; ?>" class="product-tabs__link product-tabs__link--prevlast">
							<svg class="icon-chevron-right"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#chevron-small-left"></use></svg>
						</a>
					</li>
				<?php } ?>
			</ul>
		</div>
	</div>
					<div class="catalogue__pagination_popup" style="display:none;">
						<?php echo $pagination; ?>
					</div>
	<!-- CATALOGUE PRODUCT DETAIL TABS LINKS END-->

<!-- CATALOGUE PRODUCT DETAIL END-->


<script type="text/javascript"><!--
$(document).ready(function() {
	$(".js-fancy-tab").fancybox({
//		closeTpl : '<button data-fancybox-close class="popup-simple__close">x</button>',
				slideClass : 'popup-simple--fancybox',
				autoFocus : false,
		maxWidth	: 100,
		maxHeight	: 100,
		fitToView	: false,
		autoSize	: false,
		closeClick	: false,
		closeBtn   : false,
		type: 'ajax',
		btnTpl : {
		       smallBtn   : '<button data-fancybox-close class="popup-simple__close popup-simple--fancybox" title="{{CLOSE}}">x</button>'
		      },
	});
});

//--></script>