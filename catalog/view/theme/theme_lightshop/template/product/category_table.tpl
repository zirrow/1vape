					<!-- CATALOGUE TABLE-->
					<table class="catalogue__table js-table active">
					<?php foreach ($products as $product) { ?>
						<tr class="catalogue__table-row">
							<td class="catalogue__table-photo">
							<?php if($product_detail == 1) { ?>
							<!-- Quick view when clicking on a link -->
								<a href="<?php echo $product['href']; ?>" data-for="<?php echo $product['product_id']; ?>" class="catalogue__photo-link js-product-view-btn">
									<img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>">
								</a>
							<?php } else { ?>
								<a href="<?php echo $product['href']; ?>" class="catalogue__photo-link">
									<img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>">
								</a>
							<?php } ?>
							</td>
							<td class="catalogue__table-name">
								<?php if ($product['rating'] !== false) { ?>
								<div class="catalogue__product-rating">
									<div class="product__rating">										
										<div class="product__rating-fill" style="width: <?php echo $product['rating']*20; ?>%;">											
										</div>
									</div>
								</div>
								<?php } ?>
								<?php if($product_detail == 1) { ?>
								<!-- Quick view when clicking on a link -->
								<a href="<?php echo $product['href']; ?>" data-for="<?php echo $product['product_id']; ?>" class="catalogue__product-name js-product-view-btn">
									<?php echo $product['name']; ?>
								</a>
								<?php } else { ?>
								<a href="<?php echo $product['href']; ?>" class="catalogue__product-name">
									<span><?php echo $product['name']; ?></span>
								</a>
								<?php } ?>
							</td>
							<td class="catalogue__table-price">
							<?php if ($product['price']) { ?>
							<?php if ($product['special']) { ?>
								<span class="catalogue__price-old">
									<?php echo $product['price']; ?>
								</span>
								<span class="catalogue__price catalogue__price--md">
									<?php echo $product['special']; ?>
								</span>
							<?php } else { ?>	
								<span class="catalogue__price catalogue__price--md">
									<?php echo $product['price']; ?>
								</span>
							<?php } ?>	
							<?php } ?>	
							</td>
							<td class="catalogue__table-btn">
								<a class="btn btn--transparent catalogue__btn-cart" data-for="<?php echo $product['product_id']; ?>" title="<?php echo $button_cart; ?>" >
									<span class="catalogue__btn-cart-plus"> + </span>
									<svg class="icon-cart">
										<use xlink:href="#shopping-cart"></use>
									</svg>
								</a>
							</td>
							<td class="catalogue__table-spinner">
								<div class="spinner-wrap">
									<input type="text" class="spinner" value='<?php echo $product['minimum']; ?>' placeholder="<?php echo $product['minimum']; ?>">
								</div>
							</td>
							<td class="catalogue__table-action">
								<span class="catalogue__table-action-item">
									<a class="catalogue__table-action-link" onclick="wishlist.add('<?php echo $product['product_id']; ?>');">
										<span class="catalogue__table-action-icon">
											<svg class="icon-heart icon-heart--gray">
												<use xlink:href="#heart"></use>
											</svg>
										</span>
										<span class="catalogue__table-action-text">
											<?php echo $button_wishlist; ?>
										</span>
									</a>
								</span>
								<span class="catalogue__table-action-item">
									<a class="catalogue__table-action-link" onclick="compare.add('<?php echo $product['product_id']; ?>');">
										<span class="catalogue__table-action-icon">
											<svg class="icon-add-to-list">
												<use xlink:href="#add-to-list"></use>
											</svg>
										</span>
										<span class="catalogue__table-action-text">
											<?php echo $button_compare; ?>
										</span>
									</a>
								</span>
							</td>
						</tr>
					<?php } ?>
					</table>
					<!-- CATALOGUE TABLE END-->
