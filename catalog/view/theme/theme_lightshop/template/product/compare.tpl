<?php echo $header; $col = $column_left ? 9 : 12; $col = $column_right ? $col - 3 : $col; ?>
<main class="content">
	<div class="container">
	<?php echo $content_top; ?>
		<div class="breadcrumbs breadcrumbs--sm-pad">
			<ul class="breadcrumb__list">
			<?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
			<?php if($i == 0) { ?>
				<li class="breadcrumb__list-item"><a href="<?php echo $breadcrumb['href']; ?>"><span><?php echo $breadcrumb['text']; ?></span></a></li>
			<?php } elseif($i + 1 < count($breadcrumbs)) { ?>
				<li class="breadcrumb__list-item"><svg class="icon-chevron-right"><use xlink:href="#chevron-small-left"></use></svg><a href="<?php echo $breadcrumb['href']; ?>"><span><?php echo $breadcrumb['text']; ?></span></a></li>		
			<?php } else { ?>
				<li class="breadcrumb__list-item"><svg class="icon-chevron-right"><use xlink:href="#chevron-small-left"></use></svg><span><?php echo $breadcrumb['text']; ?></span></li>		
			<?php } ?>
			<?php } ?> 			
			</ul>
		</div>
    <h1 class="content__title"><?php echo $heading_title; ?></h1>
	<div class="row">
		<?php echo $column_left ? '<div class="col-sm-3">' . $column_left . '</div>' : ''?>
		<div class="col-sm-<?php echo $col; ?>">	
		
		
			<?php if ($products) { ?>
			<table class="table">
				<thead>
					<tr class="table__row">
						<td class="table__col" colspan="<?php echo count($products) + 1; ?>"><strong><?php echo $text_product; ?></strong></td>
					</tr>
				</thead>
				<tbody>
					<tr class="table__row">
						<td class="table__col"><?php echo $text_name; ?></td>
						<?php foreach ($products as $product) { ?>
						<td class="table__col"><a href="<?php echo $product['href']; ?>"><strong><?php echo $product['name']; ?></strong></a></td>
						<?php } ?>
					</tr>
					<tr class="table__row">
						<td class="table__col"><?php echo $text_image; ?></td>
						<?php foreach ($products as $product) { ?>
						<td class="table__col table__col--center"><?php if ($product['thumb']) { ?>
							<img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail" />
							<?php } ?></td>
						<?php } ?>
					</tr>
					<tr class="table__row">
						<td class="table__col"><?php echo $text_price; ?></td>
						<?php foreach ($products as $product) { ?>
						<td class="table__col"><?php if ($product['price']) { ?>
							<?php if (!$product['special']) { ?>
							<?php echo $product['price']; ?>
							<?php } else { ?>
							<strike><?php echo $product['price']; ?></strike> <?php echo $product['special']; ?>
							<?php } ?>
							<?php } ?></td>
						<?php } ?>
					</tr>
					<tr class="table__row">
						<td class="table__col"><?php echo $text_model; ?></td>
						<?php foreach ($products as $product) { ?>
						<td class="table__col"><?php echo $product['model']; ?></td>
						<?php } ?>
					</tr>
					<tr class="table__row">
						<td class="table__col"><?php echo $text_manufacturer; ?></td>
						<?php foreach ($products as $product) { ?>
						<td class="table__col"><?php echo $product['manufacturer']; ?></td>
						<?php } ?>
					</tr>
					<tr class="table__row">
						<td class="table__col"><?php echo $text_availability; ?></td>
						<?php foreach ($products as $product) { ?>
						<td class="table__col"><?php echo $product['availability']; ?></td>
						<?php } ?>
					</tr>
					<?php if ($review_status) { ?>
					<tr class="table__row">
						<td class="table__col"><?php echo $text_rating; ?></td>
						<?php foreach ($products as $product) { ?>
						<td class="rating"><?php for ($i = 1; $i <= 5; $i++) { ?>
							<?php if ($product['rating'] < $i) { ?>
							<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
							<?php } else { ?>
							<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
							<?php } ?>
							<?php } ?>
							<br />
							<?php echo $product['reviews']; ?></td>
						<?php } ?>
					</tr>
					<?php } ?>
					<tr class="table__row">
						<td class="table__col"><?php echo $text_summary; ?></td>
						<?php foreach ($products as $product) { ?>
						<td class="description"><?php echo $product['description']; ?></td>
						<?php } ?>
					</tr>
					<tr class="table__row">
						<td class="table__col"><?php echo $text_weight; ?></td>
						<?php foreach ($products as $product) { ?>
						<td class="table__col"><?php echo $product['weight']; ?></td>
						<?php } ?>
					</tr>
					<tr class="table__row">
						<td class="table__col"><?php echo $text_dimension; ?></td>
						<?php foreach ($products as $product) { ?>
						<td class="table__col"><?php echo $product['length']; ?> x <?php echo $product['width']; ?> x <?php echo $product['height']; ?></td>
						<?php } ?>
					</tr>
				</tbody>
				<?php foreach ($attribute_groups as $attribute_group) { ?>
				<thead>
					<tr class="table__row">
						<td class="table__col" colspan="<?php echo count($products) + 1; ?>"><strong><?php echo $attribute_group['name']; ?></strong></td>
					</tr>
				</thead>
				<?php foreach ($attribute_group['attribute'] as $key => $attribute) { ?>
				<tbody>
					<tr class="table__row">
						<td class="table__col"><?php echo $attribute['name']; ?></td>
						<?php foreach ($products as $product) { ?>
						<?php if (isset($product['attribute'][$key])) { ?>
						<td class="table__col"><?php echo $product['attribute'][$key]; ?></td>
						<?php } else { ?>
						<td class="table__col"></td>
						<?php } ?>
						<?php } ?>
					</tr>
				</tbody>
				<?php } ?>
				<?php } ?>
				<tr class="table__row">
					<td class="table__col"></td>
					<?php foreach ($products as $product) { ?>
					<td class="table__col"><input type="button" value="<?php echo $button_cart; ?>" class="btn btn-primary btn-block" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');" />
						<a href="<?php echo $product['remove']; ?>" class="btn btn-danger btn-block"><?php echo $button_remove; ?></a></td>
					<?php } ?>
				</tr>
			</table>
			<?php } else { ?>
			<p><?php echo $text_empty; ?></p>
			<a href="<?php echo $continue; ?>" class="btn"><?php echo $button_continue; ?></a>
			<?php } ?>
		</div>
		<?php echo $column_right ? '<div class="col-sm-3">' . $column_right . '</div>' : ''?>
	</div>	
	<?php echo $content_bottom; ?>
	</div>
</main>
<?php echo $footer; ?></body></html>