
					<!-- CATALOGUE LISTS-->
					<div  class="catalogue__products-lists js-lists <?php echo $viewSub; ?> active">
						<ul class="catalogue__products-list">
						<?php foreach ($products as $product) { ?>
							<li class="catalogue__products-list-item">
							  <?php if(!$product['images']) { ?>
								<div class="products-list__link" >
									<?php if($product_detail == 1) { ?>
									<!-- Quick view when clicking on a link -->
										<a data-for="<?php echo $product['product_id']; ?>" href="<?php echo $product['href']; ?>" class="products-list__img js-product-view-btn">
											<img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>">
										</a>
									<?php } else { ?>
										<a class="products-list__img" href="<?php echo $product['href']; ?>">
											<img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>">
										</a>
									<?php } ?>
									<?php if($product['isnewest']) { ?>
										<span class="label products-list__label"><?php echo $labelsinfo['new']['name'][$language_id]; ?></span>
									<?php } ?>
									<?php if($product['special'] && $product['sales']) { ?>
										<span class="label products-list__label label--red"><?php echo $labelsinfo['sale']['name'][$language_id] . ' ' . $product['discount']; ?></span>
									<?php } ?>
									<div class="products-list__caption">
										<?php if ($product['rating'] !== false) { ?>
										<div class="product__rating product__rating--sm">
											<div class="product__rating-fill" style="width: <?php echo $product['rating']*20; ?>%;"></div>
										</div>
										<?php } ?>
										<?php if($product_detail == 1) { ?>
										<!-- Quick view when clicking on a link -->
										<a data-data-for="<?php echo $product['product_id']; ?>" href="<?php echo $product['href']; ?>" class="catalogue__product-name products-list__name js-product-view-btn"><span><?php echo $product['name']; ?></span></a>
										<?php } else { ?>
										<a href="<?php echo $product['href']; ?>" class="catalogue__product-name products-list__name"><span><?php echo $product['name']; ?></span></a>
										<?php } ?>
									<?php if ($product['special']) { ?>
										<span class="catalogue__price-old catalogue__price-old--sm"><?php echo $product['price']; ?></span>
										<span class="catalogue__price catalogue__price--sm"><?php echo $product['special']; ?></span>
									<?php } else { ?>	
										<span class="catalogue__price catalogue__price--sm"><?php echo $product['price']; ?></span>
									<?php } ?>	
									</div>
									<span class="products-list__like" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');">
										<svg class="icon-heart icon-heart--gray">
											<use xlink:href="#heart"></use>
										</svg>
									</span>
									<div class="products-list__action">
										<div class="products-list__btns">
										<?php if($product_detail == 2) { ?>
										<!-- Quick view when clicking on a link -->
											<span class="products-list__btns-item">
												<a data-for="<?php echo $product['product_id']; ?>" class="btn products-list__btn js-product-view-btn">
													<svg class="icon-eye">
														<use xlink:href="#eye"></use>
													</svg>
												</a>
											</span>
										<?php } ?>
											<span class="products-list__btns-item">
												<button type="button" class="products-list__btn <?php echo $product_detail == 2 ? '' : 'products-list__btn--lg'?>" title="<?php echo $button_cart; ?>" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');">
												<svg class="icon-cart">
													<use xlink:href="#shopping-cart"></use>
												</svg>
												</button>
											</span>
										</div>
										<a class="products-list__compare" onclick="compare.add('<?php echo $product['product_id']; ?>');">
											<span class="products-list__compare-icon">
												<svg class="icon-add-to-list">
													<use xlink:href="#add-to-list"></use>
												</svg>
											</span>
											<span class="products-list__compare-text">
												<?php echo $button_compare; ?>
											</span>
										</a>
									</div>
								</div>
							  <?php }else{ ?>
						<!-- Product with image slider -->

									<div  class="products-list__link js-btn-hover-parent">
										<div class="products-list__img js-simple-slider">
												<a <?php /* Quick view when clicking on a link */ echo $product_detail == 1 ? 'data-for="' . $product['product_id'] . '"' : ''?> href="<?php echo $product['href']; ?>" class="products-list__img-item <?php echo $product_detail == 1 ? 'js-product-view-btn' : ''?>">
													<img src="<?php echo $product['thumb']; ?>" alt="" class="js-img-hover">
												</a>
											<?php foreach ($product['images'] as $imageX) { ?>
												<a <?php /* Quick view when clicking on a link */ echo $product_detail == 1 ? 'data-for="' . $product['product_id'] . '"' : ''?> href="<?php echo $product['href']; ?>" class="products-list__img-item <?php echo $product_detail == 1 ? 'js-product-view-btn' : ''?>">
													<img data-lazy="<?php echo $imageX; ?>" src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs%3D" alt="" class="js-img-hover">
												</a>
											<?php } ?>
										</div>
									<?php if($product['isnewest']) { ?>
										<span class="label products-list__label"><?php echo $labelsinfo['new']['name'][$language_id]; ?></span>
									<?php } ?>
									<?php if($product['special'] && $product['sales']) { ?>
										<span class="label products-list__label label--red"><?php echo $labelsinfo['sale']['name'][$language_id] . ' ' . $product['discount']; ?></span>
									<?php } ?>
										<div class="products-list__caption">
											<?php if ($product['rating'] !== false) { ?>
											<div class="product__rating product__rating--sm">
												<div class="product__rating-fill" style="width: <?php echo $product['rating']*20; ?>%;">
												</div>
											</div>
											<?php } ?>
											<?php if($product_detail == 1) { ?>
											<!-- Quick view when clicking on a link -->
											<a data-for="<?php echo $product['product_id']; ?>" href="<?php echo $product['href']; ?>" class="catalogue__product-name products-list__name js-product-view-btn"><span><?php echo $product['name']; ?></span></a>
											<?php } else { ?>
											<a href="<?php echo $product['href']; ?>" class="catalogue__product-name products-list__name"><span><?php echo $product['name']; ?></span></a>
											<?php } ?>
										<?php if ($product['special']) { ?>
											<span class="catalogue__price-old catalogue__price-old--sm"><?php echo $product['price']; ?></span>
											<span class="catalogue__price catalogue__price--sm"><?php echo $product['special']; ?></span>
										<?php } else { ?>	
											<span class="catalogue__price catalogue__price--sm"><?php echo $product['price']; ?></span>
										<?php } ?>
										</div>
										<span class="products-list__like" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');">
										<svg class="icon-heart icon-heart--gray">
											<use xlink:href="#heart"></use>
										</svg>
										</span>
										<div class="products-list__action js-btn-clear">
											<div class="products-list__btns">
												<?php if($product_detail == 2) { ?>
												<!-- Quick view when clicking on a link -->
													<span class="products-list__btns-item">
														<a data-for="<?php echo $product['product_id']; ?>" class="btn products-list__btn js-product-view-btn">
														<svg class="icon-eye">
															<use xlink:href="#eye"></use>
														</svg>
														</a>
													</span>
												<?php } ?>
												<span class="products-list__btns-item">
													<button type="button" class="products-list__btn <?php echo $product_detail == 2 ? '' : 'products-list__btn--lg'?>" title="<?php echo $button_cart; ?>" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');">
													<svg class="icon-cart">
														<use xlink:href="#shopping-cart"></use>
													</svg>
													</button>
												</span>
											</div>
											<a class="products-list__compare" onclick="compare.add('<?php echo $product['product_id']; ?>');">
												<span class="products-list__compare-icon">
												<svg class="icon-add-to-list">
													<use xlink:href="#add-to-list"></use>
												</svg>
												</span>
												<span class="products-list__compare-text">
													<?php echo $button_compare; ?>
												</span>
											</a>
										</div>
									</div>
							  
							  <?php } ?>
							</li>
						<?php } ?>
						</ul>
					</div>
					<!-- CATALOGUE LISTS END-->
