<?php echo $header; $col = $column_left ? 9 : 12; $col = $column_right ? $col - 3 : $col; ?>
	<main <?php echo $schema ? 'itemscope itemtype="http://schema.org/Product"' : ''?> class="content">

		<div class="container">
			<div class="breadcrumbs breadcrumbs--sm-pad">
				<ul class="breadcrumb__list">
				<?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
				<?php if($i == 0) { ?>
					<li <?php echo $schema ? 'itemscope itemtype="http://data-vocabulary.org/Breadcrumb"' : ''?> class="breadcrumb__list-item"><a href="<?php echo $breadcrumb['href']; ?>" <?php echo $schema ? 'itemprop="url"' : ''?>><span <?php echo $schema ? 'itemprop="title"' : ''?>><?php echo $breadcrumb['text']; ?></span></a></li>
				<?php } elseif($i + 1 < count($breadcrumbs)) { ?>
					<li <?php echo $schema ? 'itemscope itemtype="http://data-vocabulary.org/Breadcrumb"' : ''?> class="breadcrumb__list-item"><svg class="icon-chevron-right"><use xlink:href="#chevron-small-left"></use></svg><a data-href="#categories-popup<?php echo $i; ?>" class="js-popup-call-hover breadload" data-id="<?php echo $breadcrumb['cat_id']; ?>" data-i="<?php echo $i; ?>" href="<?php echo $breadcrumb['href']; ?>" <?php echo $schema ? 'itemprop="url"' : ''?>><span <?php echo $schema ? 'itemprop="title"' : ''?>><?php echo $breadcrumb['text']; ?></span></a></li>		
				<?php } else { ?>
					<li class="breadcrumb__list-item"><svg class="icon-chevron-right"><use xlink:href="#chevron-small-left"></use></svg><?php echo $breadcrumb['text']; ?></li>		
				<?php } ?>

				<?php } ?> 			
				</ul>
			</div>
				<?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
					<div class="popup" id="categories-popup<?php echo $i; ?>">
					</div>
				<?php } ?> 
	<div class="row">
		<?php echo $column_left ? '<div class="col-sm-3">' . $column_left . '</div>' : ''?>
		<div class="col-sm-<?php echo $col; ?>">
			<div class="row product-page">
				<form>
					<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
						<div class="product__img product-page__img">
							
									<?php if($isnewest) { ?>
										<div class="label"><?php echo $labelsinfo['new']['name'][$language_id]; ?></div>
									<?php } ?>
									<?php if($special && $sales) { ?>
										<span class="label products-list__label label--red"><?php echo $labelsinfo['sale']['name'][$language_id] . ' ' . $discount; ?></span>
									<?php } ?>
							<span onclick="wishlist.add('<?php echo $product_id; ?>');" title="<?php echo $button_wishlist; ?>" class="products-list__like">
								<svg class="icon-heart icon-heart--gray">
									<use xlink:href="#heart"></use>
								</svg>
							</span>
							<div class="product-page__img-slider js-preview-img">
							<?php if ($thumb) { ?>
								<div class="product-page__img-slider-item"><a href="<?php echo $popup; ?>" <?php echo $zoom ? 'class="cloud-zoom"' : 'class="js-fancy-img" data-fancybox="gallery"'?>><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" class="product-page__img-image "></a></div>
							<?php } ?>
							<?php if($images) { ?>
								<?php foreach ($images as $image) { ?>
								<div class="product-page__img-slider-item"><a href="<?php echo $image['popup']; ?>" <?php echo $zoom ? 'class="cloud-zoom"' : 'class="js-fancy-img" data-fancybox="gallery"'?>><img src="<?php echo $image['thumb']; ?>" alt="<?php echo $heading_title; ?>" class="product-page__img-image"></a></div>
								<?php } ?>	
							<?php } ?>	
							</div>

							<div class="product-page__preview-slider <?php echo $images ? '' : 'product-page__preview-slider--single'?> js-preview-slider">
								<?php if ($thumb) { ?>
									<div class="product-page__preview-slider-item"><img src="<?php echo $additional; ?>" alt="<?php echo $heading_title; ?>"></div>
								<?php } ?>
								<?php if ($images) { ?>
									<?php foreach ($images as $image) { ?>
									<div class="product-page__preview-slider-item"><img src="<?php echo $image['additional']; ?>" alt="<?php echo $heading_title; ?>"></div>
									<?php } ?>	
								<?php } ?>	
							</div>	

						</div>						
					</div>
					<div id="product" <?php echo $column_left || $column_right ? 'class="col-lg-7 col-md-7 col-sm-7 col-xs-12"' : 'class="col-lg-5 col-md-7 col-sm-7 col-xs-12"'?>>						
						<?php if ($model) { ?>
						<span class="product-page__vendor-code" <?php echo $column_left || $column_right ? 'style="position: static;max-width: inherit;"' : ''?>><?php echo $text_model; ?> <?php echo $model; ?></span>
						<?php } ?>
						<?php if ($manufacturer) { ?>
						<span class="products-full-list__series">
							 <a title="<?php echo $text_manufacturer . ' ' . $manufacturer; ?>" href="<?php echo $manufacturers; ?>"><?php echo $manufacturer; ?></a>
						</span>
						<?php } ?>
				
						<div class="products-full-list__title">
							<div class="products-full-list__name">
								<h1 <?php echo $schema ? 'itemprop="name"' : ''?> class="catalogue__product-name">
									<?php echo $heading_title; ?>
								</h1>
							</div>
							<div <?php echo $schema && $rating ? 'itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating"' : ''?> class="products-full-list__rating">
								<?php if ($schema && $rating) { ?>
								<meta itemprop="ratingValue" content="<?php echo $rating; ?>">
								<meta itemprop="reviewCount" content="<?php echo preg_replace('/[^0-9]/', '', $reviews); ?>">
								<meta itemprop="bestRating" content="5">
								<meta itemprop="worstRating" content="1">
								<?php } ?>
								<?php if ($review_status) { ?>
								<div class="product__rating">
									<div class="product__rating-fill" style="width: <?php echo 20*$rating; ?>%;" ></div>
								</div>
								<?php } ?>
							</div>
							<span class="products-full-list__compare">
								<a onclick="compare.add('<?php echo $product_id; ?>');" class="products-full-list__compare-link">
									<span class="products-full-list__compare-icon">
										<svg class="icon-add-to-list">
											<use xlink:href="#add-to-list"></use>
										</svg>
									</span>
									<span class="products-full-list__compare-text">
										<?php echo $button_compare; ?>
									</span>
								</a>
							</span>
						</div>
						
						<?php if ($short_descr) { ?>
						<div class="products-full-list__text">
							<div class="short-descr"><?php echo $shortdescription; ?></div>
							<a href="#" class="link--dashed products-full-list__text-more js-product-text-link"><?php echo $text_lightshop_products_text_more; ?></a>
						</div>
						<?php } ?>
						
						<div <?php echo $schema ? 'itemprop="offers" itemscope itemtype="http://schema.org/Offer"' : ''?> class="product-page__actions product-page__actions--bordered products-qty-info">
							<?php if ($price) { ?>
							<div class="products-full-list__price">
							<?php if (!$special) { ?>
								<span class="catalogue__price catalogue__price--lg">
									<span <?php echo $schema ? 'itemprop="price"' : ''?> content="<?php echo $price_schema; ?>"><?php echo $price; ?></span><span <?php echo $schema ? 'itemprop="priceCurrency"' : ''?> content="RUB"></span>
								</span>
							<?php } else { ?>
								<span class="catalogue__price-old catalogue__price-old--lg">
									<?php echo $price; ?>
								</span>
								<span class="catalogue__price catalogue__price--lg">
									<span <?php echo $schema ? 'itemprop="price"' : ''?> content="<?php echo $special_schema; ?>"></span><?php echo $special; ?><span <?php echo $schema ? 'itemprop="priceCurrency"' : ''?> content="RUB"></span>
								</span>
							<?php } ?>
							</div>
							<?php } ?>
							<div class="products-full-list__spinner">
								<div class="spinner-wrap products-qty-info-spinner">
									<input type="text" name="quantity" class="spinner" value="<?php echo $minimum; ?>"  placeholder="<?php echo $minimum; ?>" id="input-quantity">
								</div>
							</div>
							<div class="products-full-list__action">
								<span class="products-full-list__status status <?php echo $quantity > 0 ? 'instock' : ''?>">
								<?php if ($quantity > 0) { ?>
									<?php echo $schema ? '<link itemprop="availability" href="http://schema.org/InStock">' : ''?>
								<?php } else { ?>
									<?php echo $schema ? '<link itemprop="availability" href="http://schema.org/OutOfStock" />' : ''?>
								<?php } ?>
									<?php echo $stock; ?>
								</span>
								<input type="hidden" name="product_id" value="<?php echo $product_id; ?>">
								<span class="products-full-list__action-btn">
								<button type="button" class="btn products-full-list__btn js-btn-add-cart"><?php echo $button_cart; ?></button>
								</span>
								<?php if ($buy_click['status']) { ?>
									<a href="#popup-buy-click" data-for="<?php echo $product_id; ?>" data-typefrom="product" class="products-full-list__action-link js-fancy-popup-cart"><?php echo $text_lightshop_buy_click; ?></a>
								<?php } ?>
							</div>

						</div>
							<?php if ($price) { ?>
								<?php if ($tax || $points) { ?>
								<div class="product-page__actions">
									<?php if ($tax) { ?>
									<div class="product-page__input-box"><span class="product-page__input-box-title"><?php echo $text_tax; ?></span><?php echo $tax; ?></div>
									<?php } ?>
									<?php if ($points) { ?>
									<div class="product-page__input-box"><span class="product-page__input-box-title"><?php echo $text_points; ?></span><?php echo $points; ?></div>
									<?php } ?>
								</div>	
								<?php } ?>							
								<?php if ($discounts) { ?>
								<div class="product-page__actions">
								<?php foreach ($discounts as $discount) { ?>
								<div class="product-page__input-box"><span class="product-page__input-box-title"><?php echo $discount['quantity']; ?><?php echo $text_discount; ?></span><?php echo $discount['price']; ?></div>
								<?php } ?>
								</div>
								<?php } ?>
							<?php } ?>	


					<?php if (isset($customTabs['popup'])) { ?>
						<div class="product-page__actions product-page__actions--popup">
							<?php foreach ($customTabs['popup'] as $key => $popup) { ?>
								<a href="#custom-popup-<?php echo $key; ?>" class="product-page__actions-link--dashed js-fancy-popup"><?php echo $popup['title'] ; ?></a>
							<?php } ?>
						</div>
					<?php } ?>

						
						<?php if ($options) { ?>
							<?php $t = 1;// $mode = 1; ?>
							<?php foreach ($options as $i=> $option) { ?>
								<?php if ($t%2 != 0 || $optMode) { ?>
								
							<div class="product-page__actions">
								<?php } ?>	
								<?php if ($option['type'] == 'select') { ?>
								<div class="product-page__input-box">
									<span class="product-page__input-box-title"><?php echo ($option['required'] ? '<span style="color: red;">* </span>' : ''); ?><?php echo $option['name']; ?></span>
									<div class="select">
										<select  name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="select" data-placeholder="<?php echo $text_select; ?>">
											<option value=""><?php echo $text_select; ?></option>
											<?php foreach ($option['product_option_value'] as $option_value) { ?>
											<option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
											<?php if ($option_value['price']) { ?>
											\<?php echo $option_value['price_prefix']; ?> <?php echo $option_value['price']; ?>
											<?php } ?>
											</option>
											<?php } ?>
										</select>
									</div>
								</div>
								<?php } ?>
								<?php if ($option['type'] == 'radio') { ?>
								<div class="product-page__input-box">
									<span class="product-page__input-box-title"><?php echo ($option['required'] ? '<span style="color: red;">* </span>' : ''); ?><?php echo $option['name']; ?></span>
									<div id="input-option<?php echo $option['product_option_id']; ?>">
									<?php foreach ($option['product_option_value'] as $option_value) { ?>
										<?php if ($option_value['image']) { ?>
										<div class="radiobox-colors__list-item option-value-id-<?php echo $option_value['option_value_id']; ?>">
											<input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" id="close-type-<?php echo $option_value['product_option_value_id']; ?>">
											<label title="<?php echo $option_value['name']; ?>" for="close-type-<?php echo $option_value['product_option_value_id']; ?>" class="label label--img-<?php echo $option_value['product_option_value_id']; ?>"></label>
										</div>
										<?php } else { ?>
										<div class="product-page__radio-box option-value-id-<?php echo $option_value['option_value_id']; ?>">
											<input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" id="close-type-<?php echo $option_value['product_option_value_id']; ?>">
											<label for="close-type-<?php echo $option_value['product_option_value_id']; ?>">
											<?php echo $option_value['name']; ?>
											<?php if ($option_value['price']) { ?>
											\ <?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>
											<?php } ?>				
											</label>
										</div>
										<?php } ?>
									 <?php } ?>
									</div>
								</div>
								<?php } ?>
								<?php if ($option['type'] == 'checkbox') { ?>
								<div class="product-page__input-box">
									<span class="product-page__input-box-title"><?php echo ($option['required'] ? '<span style="color: red;">* </span>' : ''); ?><?php echo $option['name']; ?></span>
									<div id="input-option<?php echo $option['product_option_id']; ?>">
									<?php foreach ($option['product_option_value'] as $option_value) { ?>
										<?php if ($option_value['image']) { ?>
										<div class="radiobox-colors__list-item option-value-id-<?php echo $option_value['option_value_id']; ?>">
											<input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" id="close-type-<?php echo $option_value['product_option_value_id']; ?>">
											<label title="<?php echo $option_value['name']; ?>" for="close-type-<?php echo $option_value['product_option_value_id']; ?>" class="label label--img-<?php echo $option_value['product_option_value_id']; ?>"></label>
										</div>
										<?php } else { ?>
										<div class="product-page__radio-box option-value-id-<?php echo $option_value['option_value_id']; ?>">
											<input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" id="close-type-<?php echo $option_value['product_option_value_id']; ?>">
											<label for="close-type-<?php echo $option_value['product_option_value_id']; ?>">
											<?php echo $option_value['name']; ?>
											<?php if ($option_value['price']) { ?>
											\ <?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>
											<?php } ?>				
											</label>
										</div>
										<?php } ?>
									 <?php } ?>
									</div>
								</div>
								<?php } ?>							
								<?php if ($option['type'] == 'text') { ?>
								<div class="product-page__input-box">
									<span class="product-page__input-box-title"><?php echo ($option['required'] ? '<span style="color: red;">* </span>' : ''); ?><?php echo $option['name']; ?></span>
									<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>">
								</div>
								<?php } ?>
								<?php if ($option['type'] == 'textarea') { ?>
								<div class="product-page__input-box">
									<span class="product-page__input-box-title"><?php echo ($option['required'] ? '<span style="color: red;">* </span>' : ''); ?><?php echo $option['name']; ?></span>
									<textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['value']; ?></textarea>
								</div>
								<?php } ?>
								<?php if ($option['type'] == 'file') { ?>
								<div class="product-page__input-box">
									<span class="product-page__input-box-title"><?php echo ($option['required'] ? '<span style="color: red;">* </span>' : ''); ?><?php echo $option['name']; ?></span>
									<button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" class="btn btn--transparent btn--sm"><?php echo $button_upload; ?></button>
									<input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
								</div>
								<?php } ?>
								<?php if ($option['type'] == 'date') { ?>
								<div class="product-page__input-box">
									<span class="product-page__input-box-title"><?php echo ($option['required'] ? '<span style="color: red;">* </span>' : ''); ?><?php echo $option['name']; ?></span>
									<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="date">
								</div>
								<?php } ?>
								<?php if ($option['type'] == 'datetime') { ?>
								<div class="product-page__input-box">
									<span class="product-page__input-box-title"><?php echo ($option['required'] ? '<span style="color: red;">* </span>' : ''); ?><?php echo $option['name']; ?></span>
									<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="datetime">
								</div>
								<?php } ?>
								<?php if ($option['type'] == 'time') { ?>
								<div class="product-page__input-box">
									<span class="product-page__input-box-title"><?php echo ($option['required'] ? '<span style="color: red;">* </span>' : ''); ?><?php echo $option['name']; ?></span>
									<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="time">
								</div>
								<?php } ?>
								<?php if ($t%2 == 0 || $optMode) { ?>								
							</div>
								<?php } ?>
							<?php $t++; ?>
							<?php } ?>
							<?php if ($t%2 == 0 || $optMode) { ?>								
							</div>
							<?php } ?>
						<?php } ?>
						<?php if ($recurrings) { ?>
							<div class="product-page__actions">
								<div class="product-page__input-box">
									<span class="product-page__input-box-title"><?php echo $text_payment_recurring; ?></span>
									<div class="select">
										<select name="recurring_id" class="select" data-placeholder="<?php echo $text_select; ?>">
											<option value=""><?php echo $text_select; ?></option>
											<?php foreach ($recurrings as $recurring) { ?>
											<option value="<?php echo $recurring['recurring_id']; ?>"><?php echo $recurring['name']; ?></option>
											<?php } ?>
										</select>
									</div>
									<div class="help-block" id="recurring-description"></div>
								</div>
							</div>
						<?php } ?>
						<?php if ($soc_share_code) { ?>
							<div class="product-page__actions product-page__actions--share-code">
								<?php echo $soc_share_code; ?>
							</div>
						<?php } ?>
					</div>
				</form>
			</div>
		</div>
		<?php echo $column_right ? '<div class="col-sm-3">' . $column_right . '</div>' : ''?>
	</div>
		</div>

		<?php if ($description || $attribute_groups || $review_status || isset($customTabs['tab'])) { ?>			
		<div class="product-info js-product-info">
			<div class="container js-tabs-box js-tabs-box-product">
				<div class="tabs">
					<ul class="tabs__list">
					
						<li class="tabs__list-item active js-tab">
							<a href="#" class="tabs__link">
								<?php echo $tab_description; ?><svg class="icon-chevron-top"><use xlink:href="#chevron-small-left"></use></svg>
							</a>
						</li>
						
					<?php if ($attribute_groups) { ?>
						<li class="tabs__list-item js-tab">
							<a href="#" class="tabs__link">
								<?php echo $tab_attribute; ?><svg class="icon-chevron-top"><use xlink:href="#chevron-small-left"></use></svg>
							</a>
						</li>
					<?php } ?>	
					<?php if ($review_status) { ?>
						<li class="tabs__list-item js-tab">
							<a href="#" class="tabs__link">
								<?php echo $tab_review; ?><svg class="icon-chevron-top"><use xlink:href="#chevron-small-left"></use></svg>
							</a>
						</li>
					<?php } ?>	
					<?php if (isset($customTabs['tab'])) { ?>
						<?php foreach ($customTabs['tab'] as $tab) { ?>
						<li class="tabs__list-item js-tab">
							<a href="#" class="tabs__link">
								<?php echo $tab['title']; ?><svg class="icon-chevron-top"><use xlink:href="#chevron-small-left"></use></svg>
							</a>
						</li>
						<?php } ?>
					<?php } ?>
					</ul>
				</div>
				<div class="product-info__tabs-container">

					<div class="tabs-content js-tab-content js-tab-descr active">
						<div class="tabs-content__title js-tab-title">
							<a href="#" class="tabs-content__title-link">
								<?php echo $tab_description; ?><svg class="icon-chevron-top"><use xlink:href="#chevron-small-left"></use></svg>
							</a>
						</div>
						<div class="tabs-content__inner js-tab-inner">
							<div class="editor" <?php echo $schema ? 'itemprop="description"' : ''?>>
								<?php echo $description; ?>
							</div>
						</div>
					</div>
					
					<?php if ($attribute_groups) { ?>
					<div class="tabs-content js-tab-content">
						<div class="tabs-content__title js-tab-title">
							<a href="#" class="tabs-content__title-link">
								<?php echo $tab_attribute; ?><svg class="icon-chevron-top"><use xlink:href="#chevron-small-left"></use></svg>
							</a>
						</div>
						<div class="tabs-content__inner js-tab-inner">
						<?php if (!$typeOptAtt) { ?>	
						<div class="product-info__table">						
							<?php foreach ($attribute_groups as $attribute_group) { ?>
								<div class="product-info__att">
									<div class="product-info__att-top"><?php echo $attribute_group['name']; ?></div>
								</div>
								<?php foreach (array_chunk($attribute_group['attribute'], ceil(count($attribute_group['attribute']) / 2)) as $attribute_group['attribute']) { ?>
									<div class="product-info__col">
										<dl class="product-info__list">
											<?php foreach ($attribute_group['attribute'] as $attribute) { ?>
												<dt class="product-info__list-term"><?php echo $attribute['name']; ?></dt>
												<dd class="product-info__list-desc"><?php echo $attribute['text']; ?></dd>
											<?php } ?>
										</dl>
									</div>
								<?php } ?>
							<?php } ?>	
						</div>
						<?php } else { ?>
						<div class="product-info__table-v2">		
							<table class="table table-bordered">
								<?php foreach ($attribute_groups as $attribute_group) { ?>
								<thead><tr><td colspan="2"><?php echo $attribute_group['name']; ?></td></tr></thead>
								<tbody>
									<?php foreach ($attribute_group['attribute'] as $attribute) { ?>
									<tr><td><?php echo $attribute['name']; ?></td><td><?php echo $attribute['text']; ?></td></tr>
									<?php } ?>
								</tbody>
								<?php } ?>
							</table>
						</div>
						<?php } ?>

						</div>
					</div>
					<?php } ?>
					
					<?php if ($review_status) { ?>
					<div class="tabs-content js-tab-content">
						<div class="tabs-content__title js-tab-title">
							<a href="#" class="tabs-content__title-link">
								<?php echo $tab_review; ?><svg class="icon-chevron-top"><use xlink:href="#chevron-small-left"></use></svg>
							</a>
						</div>
						<div class="product-info__feedback tabs-content__inner  js-tab-inner">
						<div id="review"></div>
						<a href="#review-popup" class="btn js-fancy-popup"><?php echo $text_lightshop_products_review; ?></a>	
						</div>
					</div>
					<?php } ?>

					<?php if (isset($customTabs['tab'])) { ?>
						<?php foreach ($customTabs['tab'] as $tab) { ?>
							<div class="tabs-content js-tab-content js-tab-descr">
								<div class="tabs-content__title js-tab-title">
									<a href="#" class="tabs-content__title-link">
										<?php echo $tab['title']; ?><svg class="icon-chevron-top"><use xlink:href="#chevron-small-left"></use></svg>
									</a>
								</div>
								<div class="tabs-content__inner js-tab-inner">
									<div class="editor">
										<?php echo html_entity_decode($tab['description'], ENT_QUOTES, 'UTF-8'); ?>
									</div>
								</div>
							</div>
						<?php } ?>
					<?php } ?>
					
				</div>
			</div>	
		</div>			
		<?php } ?>	
		
		<?php if ($products) { ?>
		<div class="recomend">
			<div class="container">
				<h2><?php echo $text_related; ?></h2>
				<div class="recomend-slider js-recom-slider">
					<?php foreach ($products as $product) { ?>
					<div class="recomend-slider__item product product--min">
						<div class="product__img">
							<a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"></a>
						</div>
						<div class="product__descr">
							<?php if ($product['rating'] !== false) { ?>
							<div class="product__rating product__rating--sm">
								<div class="product__rating-fill" style="width: <?php echo 20*$product['rating']; ?>%;">
								</div>
							</div>
							<?php } ?>
							<a href="<?php echo $product['href']; ?>" class="product__descr-link link--brown"><span><?php echo $product['name']; ?></span></a>
							<?php if ($product['price']) { ?>
								<?php if ($product['special']) { ?>
								<span class="product__price-old">
									<?php echo $product['price']; ?>
								</span>
								<span class="product__price">
									<?php echo $product['special']; ?>
								</span>
								<?php } else { ?>
								<span class="product__price"><?php echo $product['price']; ?></span>
								<?php } ?>							
							
							<?php } ?>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
		<?php } ?>
	
		<div class="popup-simple" id="review-popup">
			<div class="popup-simple__inner">
				<form id="form-review">
					<h3><?php echo $text_write; ?></h3>
					<?php if ($review_guest) { ?>					
					<div class="popup-simple__rating">
					<span><?php echo $entry_rating; ?></span>
						<div class="popup-rating">							
							<?php for ($i=5;$i>=1;$i--){ ?>
							<input class="popup-rating__input" type="radio" name="rating" value="<?php echo $i; ?>" id="star<?php echo $i; ?>" />
							<label class="popup-rating__star" for="star<?php echo $i; ?>"></label>
							<?php } ?>
						</div>				
					</div>				
					<input name="name" id="input-name" type="text" placeholder="<?php echo $entry_name; ?>" value="<?php echo $customer_name; ?>">
					<textarea rows="5" name="text" id="input-review" placeholder="<?php echo $entry_review; ?>"></textarea>
					<?php echo $captcha; ?>
					<button type="button" class="btn" onclick="review.add('<?php echo $product_id; ?>');"><?php echo $button_continue; ?></button>
					<?php } else { ?>
					<?php echo $text_login; ?>
					<?php } ?>
				</form>			
			</div>
		</div>
		<?php if ($buy_click['status']) { ?>
		<div class="popup-simple" id="popup-buy-click">
			<div class="popup-simple__inner" >
				<form>
				<h3><?php echo $text_lightshop_buy_click; ?></h3>
					<?php require( DIR_TEMPLATE . 'theme_lightshop/template/product/buyclick_form.tpl' ); ?>

					<button type="button" class="btn js-btn-add-cart-fast quickbuy-send"><?php echo $button_fastorder_sendorder; ?></button>
					<input name="quantity" id="cat_qty" type="hidden"  value="2">
					<input name="product_id" id="cat_prod_id" type="hidden"  value="">
					<?php if ($text_lightshop_pdata) { ?>
					<div class="popup-simple__inner-personal-data"><?php echo $text_lightshop_pdata; ?></div>
					<?php } ?>
				</form>
			</div>	
		</div>
		<?php } ?>
		<?php if (isset($customTabs['popup'])) { ?>
			<?php foreach ($customTabs['popup'] as $key => $popup) { ?>
				<div class="popup-simple" id="custom-popup-<?php echo $key; ?>">
					<div class="popup-simple__inner">
						<form>
							<h3><?php echo $popup['title'] ; ?></h3>				
							<div class="popup-simple__inner-text"><?php echo html_entity_decode($popup['description'], ENT_QUOTES, 'UTF-8') ; ?></div>
						</form>			
					</div>
				</div>
			<?php } ?>
		<?php } ?>
		
		<?php echo $content_bottom; ?>
		
		<?php if ($tags) { ?>
		<div class="container">
			<div class="recomend__tags">
				<span><?php echo $text_tags; ?></span>
					<?php for ($i = 0; $i < count($tags); $i++) { ?>
					<?php if ($i < (count($tags) - 1)) { ?>
					<a href="<?php echo str_replace(' ','%20',$tags[$i]['href']); ?>"><?php echo $tags[$i]['tag']; ?></a>,
					<?php } else { ?>
					<a href="<?php echo str_replace(' ','%20',$tags[$i]['href']); ?>"><?php echo $tags[$i]['tag']; ?></a>
					<?php } ?>
					<?php } ?>
			</div>
		</div>
		<?php } ?>	
		
	</main>
<?php echo $footer; ?>
<script>
$(document).ready(function() {
   breadLoad();
});
</script>

<?php if ($options) { ?>
<script>
$('<style type="text/css">').html('<?php foreach ($options as $option) {
 if ($option['type'] == 'radio' || $option['type'] == 'checkbox') {
 foreach ($option['product_option_value'] as $option_value) {
 if ($option_value['image']) { ?>.radiobox-colors__list-item input[type="radio"]+ label.label--img-<?php echo $option_value['product_option_value_id']; ?>:before, .radiobox-colors__list-item input[type="checkbox"]+ label.label--img-<?php echo $option_value['product_option_value_id']; ?>:before { background-image: url(<?php echo $option_value['image']; ?>);}<?php }
}
}
} ?>').appendTo('head');
</script>
<?php } ?>
</body></html>