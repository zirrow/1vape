<?php echo $header; $col = $column_left ? 9 : 12; $col = $column_right ? $col - 3 : $col; ?>
<main class="content">
	<div class="container">
	<?php echo $content_top; ?>
	<div class="row">
		<?php echo $column_left ? '<div class="col-sm-3">' . $column_left . '</div>' : ''?>
		<div class="col-sm-<?php echo $col; ?>">	
		<div class="error-404">
			<div class="row">
				<div class="col-md-8 col-sm-8 col-xs-12 col-md-offset-2 col-sm-offset-2">
					<div class="error-404__inner">
					<img src="catalog/view/theme/theme_lightshop/img/404.png">
						<div class="error__content">
						<h2 class="error__content-title">404</h2>
						<p class="error__content-text"><?php echo $text_error; ?><br><?php echo $text_404; ?></p>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
		<?php echo $column_right ? '<div class="col-sm-3">' . $column_right . '</div>' : ''?>
	</div>	
	<?php echo $content_bottom; ?>
	</div>
</main>
<?php echo $footer; ?></body></html>